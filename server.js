var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var parser = require('rssparser2');
var Client = require('node-torrent');
var client = new Client({logLevel: 'DEBUG'});

// DATABASE CONNECTION
var mongoose = require('mongoose');
mongoose.connect('mongodb://node:/node@novus.modulusmongo.net:27017/Iganiq8o');
var Episodes = require('../app/models/episodes');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;
var router = express.Router();

// ROUTING FOR API
router.use(function(req, res, next) {
  console.log('An event is currently happening...');
  next();
});

router.get('/', function(req, res){

  var options = {};
  //rss feeds
  parser.parseURL('http://www.nyaa.se/?page=rss&term=%5BHorribleSubs%5D+Naruto+Shippuuden', options, function(err, articles){
  	//res.json(articles);
  	var i = 0;
    var torrent;
  	for(var title in articles.items){
      torrent = client.addTorrent(articles.items[i]['url']);
  		console.log(articles.items[i]['url']);
  		i++;
  	}
    res.json(articles.items[i]);
  });
});


app.use('/api', router);

app.listen(port);
console.log('Server Started on port: ' + port);
