var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EpisodeSchema = new Schema({
  name: String
});

module.exports = mongoose.model('Episodes', EpisodeSchema);
